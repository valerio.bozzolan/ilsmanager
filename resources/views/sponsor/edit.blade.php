@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('sponsor.update', $object->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf

                @include('sponsor.form', ['object' => $object])

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            <form method="POST" action="{{ route('sponsor.destroy', $object->id) }}">
                @method('DELETE')
                @csrf

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-danger">Elimina</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
