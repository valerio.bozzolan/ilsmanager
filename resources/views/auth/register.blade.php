@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">
                @if( isset( $section ) )
                    Modulo di Registrazione alla<br />Sezione Locale di <strong>{{ $section->city }}</strong><br />di Italian Linux Society
                @else
                    Modulo di Registrazione a Italian Linux Society
                @endif
            </h2>

            <h3 class="text-center">
                Stai per unirti alla più grande Associazione di Promozione del Software Libero in Italia, dal 1994.
            </h3>

            <hr>

            <p>
                Associandoti ad Italian Linux Society, avrai diritto a:
            </p>
            <ul>
                <li>un indirizzo di posta <i>nome.cognome@linux.it</i> ed uno <i>nome.cognome@ils.org</i> (o simile, in caso di omonimie)</li>
                <li>un indirizzo di posta <i>nickname@linux.it</i> ed uno <i>nickname@ils.org</i> (previa disponibilità del nome scelto)</li>
                <!--
                <li>spazio web per l'hosting di applicazioni e la pubblicazione di contenuti</li>
                <li>un dominio <i>nomeascelta.linux.it</i></li>
                -->
                <li>accesso alla <a href="https://lists.linux.it/listinfo/soci" target="_blank">mailing list dei Soci</a></li>
                <li>diritto di voto durante le votazioni del prossimo consiglio direttivo e diritto a candidarsi</li>
                <li>una mailing list <i>nomeascelta@lists.linux.it</i> (su richiesta esplicita)</li>
                <li>aggregazione del proprio sito o blog su <a href="https://planet.linux.it/" target="_blank">planet.linux.it</a></li>
            </ul>

            <hr />
            <h4>Passo 1: Versamento della Quota Associativa</h4>

            <p>
                La quota di iscrizione annuale è di <b>{{ App\Config::getConfig('regular_annual_fee') }}</b> euro.<br />
                Per versare la tua quota ora, puoi usare:
            </p>
            <ul>
                <li>PayPal: accredita {{ App\Config::getConfig('regular_annual_fee') }} euro sul conto intestato a:<br /><code>direttore@linux.it</code></li>
                <li>IBAN: effettua un bonifico di {{ App\Config::getConfig('regular_annual_fee') }} euro sull'IBAN:<br /><code>IT74G0200812609000100129899</code><br />conto intestato a: "ILS Italian Linux Society"</li>
            </ul>

            <p>
                Dopo il versamento della quota riceverai una mail di conferma: se non ti arriva entro quindici giorni, o se hai dubbi o domande sulla procedura di iscrizione, scrivi a <strong>{{ App\Config::getConfig('association_email') }}</strong>.<br>
                Scrivi allo stesso indirizzo anche <a href="https://www.ils.org/info/#aderenti" target="_blank">se vuoi iscrivere la tua Associazione</a> o <a href="https://www.ils.org/sponsorizzazioni/" target="_blank">se la tua azienda vuole diventare sponsor di Italian Linux Society</a>.
            </p>

            @if(date('m') >= 10)
                <div class="alert alert-info text-center">
                    Iscrivendoti adesso, la tua quota vale già per l'anno {{ date('Y') + 1 }}!
                </div>
            @endif

            <hr />

            <h4>Passo 2: Registrazione Individuale</h4>

            <br>

            <div class="card">
                <div class="card-header">Modulo di Iscrizione a Italian Linux Society</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">Cognome</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required>

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Indirizzo E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">Nickname</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>
                                <small class="form-text text-muted">Viene usato per assegnare l'indirizzo mail <i>nickname@linux.it</i> e per accedere al gestionale interno dei soci.</small>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_date" class="col-md-4 col-form-label text-md-right">Data di Nascita</label>

                            <div class="col-md-6">
                                <input id="birth_date" type="text" class="form-control date {{ $errors->has('birth_date') ? 'is-invalid' : '' }}" name="birth_date" value="{{ old('birth_date') }}" placeholder="YYYY-MM-DD" required>

                                @if ($errors->has('birth_date'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_place" class="col-md-4 col-form-label text-md-right">Comune di Nascita</label>

                            <div class="col-md-4">
                                <input id="birth_place" type="text" class="form-control{{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" value="{{ old('birth_place') }}" required>
                                <small class="form-text text-muted">Inserisci la tua provincia se non sei nato all'estero, non lasciare <b>Estero</b>.</small>

                                @if ($errors->has('birth_place'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @include('commons.provinces', ['name' => 'birth_prov', 'value' => old('birth_prov')])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="taxcode" class="col-md-4 col-form-label text-md-right">Codice Fiscale</label>

                            <div class="col-md-6">
                                <input id="taxcode" type="text" class="form-control{{ $errors->has('taxcode') ? ' is-invalid' : '' }}" name="taxcode" value="{{ old('taxcode') }}" required>

                                @if ($errors->has('taxcode'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('taxcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address_street" class="col-md-4 col-form-label text-md-right">Indirizzo di Residenza</label>

                            <div class="col-md-6">
                                <input id="address_street" type="text" class="form-control{{ $errors->has('address_street') ? ' is-invalid' : '' }}" name="address_street" value="{{ old('address_street') }}" required>

                                @if ($errors->has('address_street'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address_street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_place" class="col-md-4 col-form-label text-md-right">Comune di Residenza</label>

                            <div class="col-md-2">
                                <input id="address_place" type="text" class="form-control{{ $errors->has('address_place') ? ' is-invalid' : '' }}" name="address_place" value="{{ old('address_place') }}" required>
                                <small class="form-text text-muted">Inserisci la tua provincia se non vivi all'estero, non lasciare <b>Estero</b>.</small>

                                @if ($errors->has('address_place'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @include('commons.provinces', ['name' => 'address_prov', 'value' => old('address_prov')])
                            </div>
                            <label for="address_zip" class="col-lg-12 col-form-label d-md-none">CAP di Residenza</label>
                            <div class="col-md-2">
                                <input id="address_zip" type="text" class="form-control{{ $errors->has('address_zip') ? ' is-invalid' : '' }}" name="address_zip" value="{{ old('address_zip') }}" placeholder="00000" required>
                            </div>
                        </div>

                        @if(App\Section::all()->isEmpty() == false)
                            <div class="form-group row">
                                <label for="address_street" class="col-md-4 col-form-label text-md-right">Sezione Locale</label>

                                <div class="col-md-6">
                                    <select id="address_street" class="form-control{{ $errors->has('section_id') ? ' is-invalid' : '' }}" name="section_id">
                                        <option value="">Nessuna</option>
                                        @foreach(App\Section::orderBy('city', 'asc')->get() as $possible_section)
                                            <option value="{{ $possible_section->id }}" @selected(isset($section) && $section->id == $possible_section->id) >{{ $possible_section->city }} ({{ $possible_section->prov }})</option>
                                        @endforeach
                                    </select>
                                    <p><small class="form-text text-muted">Selezionando la <a href="https://www.ils.org/sezionilocali" target="_blank">sezione locale</a> di riferimento, sosterrai le attività di promozione e divulgazione nella città scelta.</small></p>
                                    @if( !isset( $section ) )
                                        <p><small class="form-text text-muted">Se la tua sezione locale non compare nell'elenco, non preoccuparti. Potrai assegnarla successivamente.</small></p>
                                    @endif

                                    @if ($errors->has('section_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('section_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="statuto" name="statuto" required>
                                    <label class="form-check-label" for="statuto">
                                        Ho letto lo <a href="https://www.ils.org/statuto/" target="_blank">Statuto</a> e vi aderisco.
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="privacy" name="privacy" required>
                                    <label class="form-check-label" for="privacy">
                                        Ho letto l'<a href="https://www.ils.org/privacy/" target="_blank">Informativa Privacy</a> e la accetto.
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrati a Italian Linux Society
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .col -->
    </div><!-- .row -->
</div><!-- .container -->
@endsection
