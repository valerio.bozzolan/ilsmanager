<h2>Dati del rimborso</h2>

<div class="form-group row">
    <label for="date" class="col-sm-4 col-form-label">Data</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="date" value="{{ $object ? $object->date : date('Y-m-d') }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="amount" class="col-sm-4 col-form-label">Ammontare</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" step="0.01" name="amount" value="{{ $object ? $object->amount : '' }}" required @readonly( $object && !$object->getRefundStatus()->isEditableByUser($currentuser) )>
    </div>
</div>
@if ($currentuser->hasRole('admin'))
    <div class="form-group row">
        <label for="user_id" class="col-sm-4 col-form-label">Utente</label>
        <div class="col-sm-8">
            @include('user.select', ['name' => 'user_id', 'select' => ($object ? $object->user_id : $currentuser->id)])
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
    <div class="col-sm-8">
        @include('section.select', ['name' => 'section_id', 'select' => ($object ? $object->section_id : 0)])
    </div>
</div>
<div class="form-group row multiple-files">
    <label for="file" class="col-sm-4 col-form-label">File</label>
    <div class="col-sm-8">
        @if ($object)
            <ul>
                @foreach ($object->attachments as $attach)
                    <li><a href="{{ route('refund.download', ['file' => $object->id . '/' . $attach]) }}">{{ basename($attach) }}</a></li>
                @endforeach
            </ul>
        @endif

        <input type="file" name="file[]" autocomplete="off" {{ $object ? '' : 'required' }}>
        <small class="form-text text-muted">Si prega di allegare tutta la documentazione correlata: fatture, scontrini, biglietti...</small>
    </div>
</div>
<div class="form-group row">
    <label for="notes" class="col-sm-4 col-form-label">Note socio</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="notes" placeholder="Scrivere qui una breve descrizione delle spese affrontate e di come desideri ricevere il rimborso (fornisci un IBAN, ecc.)" required>{{ $object ? $object->notes : '' }}</textarea>
    </div>
</div>

@if( $object )
<hr />

<div class="form-group row">
    <label for="report_url" class="col-sm-4 col-form-label">Relazione Finale (URL)</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="report_url" placeholder="https://" value="{{ $object ? $object->report_url : '' }}" />
        <p>La relazione finale è una breve e graziosa pagina web che descriva a tutti come si è svolta la vostra attività. La puoi pubblicare su un qualsiasi sito (o forum ecc.) per descrivere cosa è andato bene, cosa sarebbe potuto andare meglio, come sono stati effettivamente utilizzati i fondi e descrivendo i risultati attesi e i risultati ottenuti. Non dimenticare qualche bella foto (fai punti bonus se sono in licenza libera!) e non dimenticarti di dare i giusti crediti e ringraziamenti alle persone e organizzazioni che hanno partecipato e vi hanno sostenuto moralmente o economicamente.</p>
    </div>
</div>
@endif

<h2>Dati dalla Segreteria</h2>

<div class="form-group row">
    <label for="admin_notes" class="col-sm-4 col-form-label">Note Segreteria</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="admin_notes" placeholder="Informazioni dell'amministratore riguardo lo stato di avanzamento del rimborso" {{ $currentuser->hasRole('admin') ? '' : 'readonly' }}>{{ $object ? $object->admin_notes : '' }}</textarea>
    </div>
</div>
