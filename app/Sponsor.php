<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    public function attachFile($file)
    {
        $path = self::logoPath();
        $file->move($path, $this->id);
    }

    /**
     * Check whenever this sponsor has an uploaded logo.
     * @return bool
     */
    public function hasLogo() {
        return file_exists(self::logoPath($this->id));
    }

    /**
     * Get the path to the logo of this Sponsor.
     * Specity a NULL $id to just get the directory.
     * @param $id int|null
     * @return string
     */
    public static function logoPath($id = null) {
        if ($id === null) {
            $id = '';
        }
        return storage_path('sponsors/' . $id);
    }
}
