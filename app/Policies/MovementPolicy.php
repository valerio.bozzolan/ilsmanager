<?php

namespace App\Policies;

use App\User;
use App\Movement;
use Illuminate\Auth\Access\HandlesAuthorization;

class MovementPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }
}
