<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use App;
use DB;
use Hash;

use App\User;
use App\Role;
use App\Fee;

class ImportLegacy extends Command
{
    protected $signature = 'import:legacy {database} {username} {password}';
    protected $description = 'Importa i contenuti dal vecchio ILSManager';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $factory = App::make('db.factory');

        $old_config = [
            'driver' => 'mysql',
            'host' => 'localhost',
            'username' => $this->argument('username'),
            'password' => $this->argument('password'),
            'database' => $this->argument('database'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
        ];
        $old = $factory->make($old_config);

        DB::table('users')->delete();
        DB::table('role_user')->delete();
        DB::table('password_resets')->delete();
        DB::table('movements')->delete();
        DB::table('fees')->delete();

        $current_year = date('Y');

        /***************************************************************
            Recupero utenti
        */

        $users_map = [];

        $query = 'SELECT * FROM soci_iscritti';
        $users = $old->select($query);

        echo "\n";
        $this->info('Importazione Utenti');
        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $user) {
            $u = new User();

            $u->name = $user->nome;
            $u->surname = $user->cognome;

            if (empty($user->nickname)) {
                $u->username = strtolower(sprintf('%s.%s', $u->name, $u->surname));
            }
            else {
                $u->username = $user->nickname;
            }

            $u->email = $user->email;
            $u->taxcode = $user->codfis;
            $u->type = (($user->type == 'privato' || $user->type == '') ? 'regular' : 'association');
            $u->notes = '';

            $u->birth_place = $user->comune_nasc;
            $u->birth_prov = $user->prov_nasc;
            $u->birth_date = $user->data_nasc == '0000-00-00' ? null : $user->data_nasc;

            $u->address_street = $user->indirizzo_resid;
            $u->address_place = $user->comune_resid;
            $u->address_prov = $user->prov_resid;

            $u->request_at = $user->data_domanda == '0000-00-00' ? null : $user->data_domanda;
            $u->approved_at = $user->data_ammissione == '0000-00-00' ? null : $user->data_ammissione;
            $u->expelled_at = $user->data_espulsione == '0000-00-00' ? null : $user->data_espulsione;

            if ($u->expelled_at) {
                $u->status = 'expelled';
                $u->username = Str::random(10);
            }
            else {
                if ($u->approved_at) {
                    $u->status = 'active';
                }
                else {
                    $u->status = 'pending';
                }
            }

            $u->password = Hash::make(Str::random(10));

            $u->save();

            $users_map[$user->id] = $u->id;

            $bar->advance();
        }

        $bar->finish();

        /***************************************************************
            Recupero accounts Picard
        */

        $query = 'SELECT * FROM users_picard';
        $users = $old->select($query);
        $notices = [];

        echo "\n";
        $this->info('Importazione Accounts');
        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $user) {
            $u = User::where('username', $user->nickname)->first();
            if (is_null($u)) {
                $notices[] = sprintf("L'utente %s non esiste", $user->nickname);
                continue;
            }

            if (!empty($user->fw_email)) {
                if ($u->email != $user->fw_email) {
                    $notices[] = sprintf("Modifico email utente %s: %s -> %s", $user->nickname, $u->email, $user->fw_email);
                    $u->email = $user->fw_email;
                    $u->save();
                }

                $u->setConfig('email_behaviour', 'alias');
            }
            else {
                $u->setConfig('email_behaviour', 'inbox');
                $u->setConfig('email_password', $user->pw_picard);
            }

            $bar->advance();
        }

        $bar->finish();

        echo "\n";
        foreach($notices as $n) {
            $this->error($n);
        }

        /***************************************************************
            Setto permessi
        */

        $query = 'SELECT * FROM users_perm';
        $perm = $old->select($query);
        $role = Role::where('name', '=', 'admin')->first();

        foreach ($perm as $p) {
            $u = User::where('username', '=', $p->username)->first();
            $u->roles()->attach($role->id);
        }

        /***************************************************************
            Recupero quote passate
        */

        $query = 'SELECT * FROM soci_quote WHERE YEAR(data_versamento) != ' . $current_year;
        $fees = $old->select($query);

        echo "\n";
        $this->info('Importazione Quote');
        $bar = $this->output->createProgressBar(count($fees));

        foreach ($fees as $fee) {
            if (isset($users_map[$fee->id_socio])) {
                $f = new Fee();
                $f->user_id = $users_map[$fee->id_socio];
                $f->year = $fee->anno;
                $f->save();

                $bar->advance();
            }
        }

        $bar->finish();

        /***************************************************************
            Fix prima quota
        */

        $query = "SELECT distinct(descrizione) FROM `conti_movimenti` WHERE `descrizione` IN (SELECT `descrizione` FROM `conti_movimenti` WHERE `descrizione` like 'Saldo Quota %' and YEAR(data) != $current_year GROUP BY `descrizione` HAVING COUNT(*) > 1)";
        $movements = $old->select($query);
        $notices = [];

        echo "\n";
        $this->info('Fix Quote');
        $bar = $this->output->createProgressBar(count($movements));

        foreach ($movements as $movement) {
            preg_match('/^Saldo quota (?P<surname>\w+) (?P<name>[ a-zA-Z]+).*$/', $movement->descrizione, $matches);
            $u = User::where('name', '=', trim($matches['name']))->where('surname', '=', trim($matches['surname']))->first();
            if ($u == null) {
                $notices[] = sprintf("Aggiungere manualmente una quota per utente %s %s (non trovato)", $matches['surname'], $matches['name']);
                continue;
            }

            $f = $u->fees()->orderBy('year', 'desc')->first();
            if (is_null($f)) {
                if (strncmp($u->request_at, $current_year, 4) != 0) {
                    $notices[] = sprintf("Aggiungere manualmente una quota per utente %s %s (manca prima quota)", $u->surname, $u->name);
                    continue;
                }
            }

            $f2 = new Fee();
            $f2->user_id = $u->id;
            $f2->year = $f->year + 1;
            $f2->save();

            $bar->advance();
        }

        $bar->finish();

        $missing = User::whereDoesntHave('fees')->get();
        foreach($missing as $u) {
            $query = "SELECT * FROM conti_movimenti WHERE descrizione = 'Iscrizione nuovo socio $u->surname $u->name' AND YEAR(data) != $current_year";
            $subscription = $old->select($query);

            if (empty($subscription)) {
                if ($u->status == 'active') {
                    list($year, $month, $day) = explode('-', $u->approved_at);
                    $f = new Fee();
                    $f->user_id = $u->id;
                    $f->year = $year;
                    $f->save();
                }
            }
            else {
                foreach ($subscription as $movement) {
                    list($year, $month, $day) = explode('-', $movement->data);
                    $f = new Fee();
                    $f->user_id = $u->id;
                    $f->year = $year;
                    $f->save();
                    break;
                }
            }
        }

        echo "\n";
        foreach($notices as $n) {
            $this->error($n);
        }
    }
}
