<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class CheckExpelled extends Command
{
    protected $signature = 'check:expelled';
    protected $description = 'Effettua espulsione dei soci sospesi';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::where('status', 'suspended')->get();
        $threeshold = date('Y', strtotime('-24 months'));

        foreach($users as $u) {
            $last_fee = $u->fees()->max('year');
            if ($last_fee < $threeshold) {
                $u->status = 'expelled';
                $u->save();
            }
        }

		return 0;
    }
}
