<?php
/**
    ILS Manager
    Copyright (C) 2021-2024 Italian Linux Society, contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use PayPalAPIMovementsReader;

use App\Movement;
use App\Bank;
use App\Account;
use App\AccountRow;
use App\Refund;
use App\Config;

class MovementController extends Controller
{
    public function index(Request $request)
    {
        $this->checkAuth();

        // force integer
        $year = (int) $request->input('year', date('Y'));
        if( $year < 1000 ) {
            abort( 500, "whaat" );
        }

        $objects = AccountRow::with('movement')->whereHas('movement', function($query) use ($year) {
            $query->where(DB::raw('YEAR(date)'), $year);
        })->orderBy('id', 'desc')->get()->sortByDesc(function($a) {
            return $a->movement->date;
        });

        // incomes grouped by month and year
        $incomes = DB::table('movements')
            ->selectRaw('sum(amount) as amount, date_format(date, "%Y-%m") as date')
            ->where('amount', '>', 0)
            ->where('date', '>=', $year.'-01-01')
            ->where('date', '<=', $year.'-12-31')
            ->groupBy('date')
            ->pluck('amount', 'date')->toArray();
        $incomes_per_month = [];

        for ($i = 1; $i <= 12; $i++) {
            $incomes_per_month[] = (float) ($incomes[$year.'-'.str_pad($i, 2, '0', STR_PAD_LEFT)] ?? 0);
        }


        // costs grouped by month and year
        $costs = DB::table('movements')
            ->selectRaw('sum(amount) as amount, date_format(date, "%Y-%m") as date')
            ->where('amount', '<', 0)
            ->where('date', '>=', $year.'-01-01')
            ->where('date', '<=', $year.'-12-31')
            ->groupBy('date')
            ->pluck('amount', 'date')->toArray();
        $costs_per_month = [];

        for ($i = 1; $i <= 12; $i++) {
            $costs_per_month[] = (float) ($costs[$year.'-'.str_pad($i, 2, '0', STR_PAD_LEFT)] ?? 0);
        }

        $revenue_per_month = [];
        $progress = 0;
        foreach ($incomes_per_month as $period => $income) {
            $revenue_per_month[$period] = $income + $costs_per_month[$period] + $progress;
            $progress = $revenue_per_month[$period];
        }

        return view('movement.index', [
            'objects' => $objects,
            'year' => $year,
            'incomes' => $incomes_per_month,
            'costs' => $costs_per_month,
            'revenue' => $revenue_per_month,
        ]);
    }

    /**
     * Parse and store the PayPal file with movements
     */
    public function store(Request $request)
    {
        $this->checkAuth();

	if( $request->input('store_from') === 'paypal_api' ) {

		// eventually upload via PayPal API
		$this->storeFromPayPalAPI( $request );

	} else {

		// load from file
		$this->storeFromFile( $request );
	}

        return redirect()->route('movement.review');
    }

    /**
     * Load movements from file
     */
    private function storeFromFile(Request $request)
    {
        DB::beginTransaction();
        $bank = Bank::find($request->input('bank_id'));

        // no file no party
        if(!$request->file('file')) {
            abort( 400, "Please pick a file" );
        }

        if ($request->file('file')->isValid()) {
            switch($bank->type) {
                case 'paypal':
                    throw new \Exception("Please do not import PayPal movements from file. Import from API instead.");
                    break;

                case 'unicredit':
                    $header_skipped = false;

                    $unicredit_content = file_get_contents($request->file->path());
                    $unicredit_rows = explode("\n", $unicredit_content);

                    // IMPORTANT: Do not try to consider the Unicredit's CSV like a CSV.
                    // Unicredit is not aware of RFC 4180. Don't use fgetcsv().
                    // So we try to be aggressive on first and last rows,
                    // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/58
                    // https://regex101.com/r/Vw4mK2/1
                    //                                                                                                   /- Group 3
                    //                                                                                                  /   Malicious user's summary
                    //                                       /- Group 1                   /- Group 2                   /
                    //                                      /   dd/mm/yyyy               /  date dd/mm/yyyy           /     /- Group 4   /- Group 5
                    //                                     /                            /                            /     /   Amount   /   Unicredit payment ID
                    $UNICREDIT_INSANE_COLUMN_PATTERN = '@^([0-9]{2}/[0-9]{2}/[0-9]{4});([0-9]{2}/[0-9]{2}/[0-9]{4});(.+?);(-?[0-9.,]+);([0-9]+)$@';
                    $UNICREDIT_EXPECTED_COLUMNS = 5;
                    $unicredit_something = true;
                    $unicredit_heading_found = false;
                    foreach($unicredit_rows as $i => $unicredit_row) {
                        $unicredit_row_clean = trim($unicredit_row);
                        if(empty($unicredit_row_clean)) {
                            continue;
                        }

                        // Avoid completely nonsense rows starging with this phrase.
                        if(strpos($unicredit_row_clean, "Situazione al ") === 0) {
                            continue;
                        }

                        // Avoid completely nonsense data. This only happens sometime (!)
                        if(strpos($unicredit_row_clean, "Saldo Euro") === 0) {
                            continue;
                        }

                        // Avoid completely nonsense data. This also only happens sometime (!)
                        if(strpos($unicredit_row_clean, "Ultimi movimenti") === 0) {
                            continue;
                        }

                        // Avoid partially nonsense data. This has valid columns but invalid data (heading).
                        if(strpos($unicredit_row_clean, "Data;Valuta;Descrizione") === 0) {
                            $unicredit_heading_found = true;
                            continue;
                        }

                        // Insanely parse the row.
                        $unicredit_insane_matches = [];
                        $n_matches = preg_match($UNICREDIT_INSANE_COLUMN_PATTERN, $unicredit_row_clean, $unicredit_insane_matches);
                        if($n_matches === false) {
                            throw new \Exception(sprintf("Invalid regex: %s", $UNICREDIT_INSANE_COLUMN_PATTERN));
                        }

                        // Be very strict if we have not found any heading yet. So we catch Unicredit nonsenses and we can improve the parser
                        // without causing silent import nonsenses.
                        if($n_matches !== 1) {
                            throw new \Exception(sprintf("Unexpected Unicredit row: '%s' not matching '%s'", $unicredit_row, $UNICREDIT_INSANE_COLUMN_PATTERN));
                        }

                        // The frist array row from preg_match() is just the whole line. Strip that.
                        array_shift($unicredit_insane_matches);

                        // The "$c" is alias for "columns".
                        $c = $unicredit_insane_matches;

                        $unicredit_something = true;

                        try {
                            $movement = new Movement();
                            $movement->amount = (float) $this->parseUnicreditFloatNumber($c[3]);
                            $movement->bank_id = $bank->id;
                            $movement->date = implode("-", array_reverse(explode("/", $c[0])));
                            $movement->notes = $c[2];

                            if ($movement->alreadyTracked()) {
                                continue;
                            }

                            $movement->save();
                        } catch(\Exception $e) {
                            echo $e->getMessage() . '<br>';
                            print_r($c);
                            DB::rollBack();
                            exit();
                        }
                    }

                    // No data? No heading? WTF.
                    if(!$unicredit_something && !$unicredit_heading_found) {
                        throw new \Exception("We have not found a valid Unicredit file with the expected heading.");
                    }

                    break;
            }
        }

        DB::commit();
    }

    /**
     * Load movements from PayPal API
     *
     * @author Valerio Bozzolan
     */
    private function storeFromPayPalAPI( Request $request )
    {
        // if something goes wrong this throws a cute exception
        $config = PayPalAPIMovementsReader::validatedConfig();

        // we have to know the exact PayPal bank ID
        $bank_id = $config['bankid'];

        $fee_sum = 0.0;

        // Allo user to set a custom Date From.
        $date_start = $request->input('date_from');
        if ($date_start ) {
            $date_start = \DateTime::createFromFormat('Y-m-d', $date_start );
        } else {
            // Guess a meaningful Date From.
            $latest = Movement::where('bank_id', $bank_id)->orderBy('id', 'desc')->first();
            if ($latest) {
                $date_start = \DateTime::createFromFormat('Y-m-d', $latest->date)->sub(new \DateInterval('P1D'));
            }
        }

        // Allow user to set a custom Date To.
        $date_to = $request->input('date_to');
        if( $date_to ) {
            $date_to = \DateTime::createFromFormat('Y-m-d', $date_to);
        }

        // Stuff for troubleshooting / creators of Unit tests.
        // Maybe the developer just wants to dump the PayPal results.
        if( app()->isLocal() && $request->input('debug_superlol_activator') ) {
            header("Content-type: text/plain");
            PayPalAPIMovementsReader::process( [
                'startDate' => $date_start,
                'endDate' => $date_to,
                'itemCallback' => function ($response_data, $response_internal_data) {
                    echo "============ PAYPAL RESULT (ready to be copy-paste as new unit test inside the directory /tests/Unit/paypal/ asdlol asdlol asdlol BUT REDACT PRIVATE DATA FIRST :D :D) ===========\n";
                    echo json_encode($response_internal_data['paypal_transaction'], JSON_PRETTY_PRINT) . "\n";
                    echo \Tests\Unit\PayPalTransactionImportTest::SEPARATOR_BETWEEN_TRANSACTION_AND_DETAILS . "\n";
                    echo json_encode($response_internal_data['paypal_transaction_details'], JSON_PRETTY_PRINT) . "\n";
                    echo \Tests\Unit\PayPalTransactionImportTest::SEPARATOR_BETWEEN_DETAILS_AND_RESULTS . "\n";
                    echo json_encode($response_data, JSON_PRETTY_PRINT) . "\n\n\n\n";
                }
            ] );
            exit;
        }

        DB::beginTransaction();

        PayPalAPIMovementsReader::process( [
            'startDate' => $date_start,
            'endDate' => $date_to,
            'itemCallback' => function ($response_data) use ($bank_id, $fee_sum) {
                $id        = $response_data['id'];
                $date      = $response_data['date'];
                $amount    = $response_data['amount_gross'];
                $amount_net= $response_data['amount_net']; // TODO: now unused.
                $fee       = $response_data['amount_fee'];
                $notes     = $response_data['notes'];
                $notes_xl  = $response_data['notes_xl'];

                $date = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $date);

                $movement = new Movement();
                $movement->amount = $amount;
                $movement->bank_id = $bank_id;
                $movement->identifier = $id;
                $movement->date = $date->format( 'Y-m-d' );
                $movement->notes = $notes_xl;

                // do not save twice
                $existing_movement = $movement->alreadyTrackedMovement();
                if ($existing_movement == null) {
                    $movement->save();

                    // remember fees
                    // TODO: create an AccountRow, immediately.
                    if( $fee ) {
                        $fee_sum += $fee;
                    }
                }
            },
        ] );

        // eventually process fees
        if( $fee_sum ) {
            $movement = new Movement();
            $movement->amount = $fee_sum;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->notes = 'Commissioni PayPal';
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->account_id = Account::where('bank_costs', true)->first()->id;
            $ar->notes = 'Commissioni PayPal';
            $ar->amount_in = $fee_sum;
            $ar->save();
        }

        DB::commit();
    }

    public function edit($id)
    {
        $this->checkAuth();
        $movement = Movement::find($id);
        return view('movement.edit', compact('movement'));
    }

    public function update(Request $request, $id)
    {
        $this->checkAuth();

        DB::beginTransaction();

        if ($id == 0) {
            $fees_account = Config::feesAccount();
            $rows = [];
            $fee_years = [];

            $movements = $request->input('movement');
            $account_rows = $request->input('account_row');
            $accounts = $request->input('account');
            $amounts = $request->input('amount');
            $users = $request->input('user');
            $sections = $request->input('section');
            $notes = $request->input('notes');

            $to_skip = [];

            $remove = $request->input('remove', []);
            foreach($remove as $r) {
                $m = Movement::find($r);
                $m->delete();
            }

            $movements_by_id = array();
            foreach($movements as $index => $id) {
                if (in_array($id, $to_skip)) {
                    continue;
                }

                $m = Movement::find($id);
                if (is_null($m)) {
                    continue;
                }
                $movements_by_id[$id] = $m;

                if ($amounts[$index] == 0 || $accounts[$index] == 0) {
                    continue;
                }

                if ($accounts[$index] == $fees_account->id && $users[$index] == 0) {
                    continue;
                }

                $account_row_id = $account_rows[$index];

                if ($account_row_id == 'new') {
                    /*
                        I pagamenti delle quote sono gestiti in modo speciale,
                        per accertarsi di salvare un AccountRow (da cui poi
                        verrà generato una Fee) per ogni quota versata
                    */
                    if ($accounts[$index] == $fees_account->id) {
                        $ars = $m->getFeePayments($users[$index]);

                        if (!isset($rows[$id])) {
                            $rows[$id] = [];
                        }

                        foreach($ars as $ar) {
                            $ar->save();
                            $rows[$id][] = $ar->id;
                        }

                        $to_skip[] = $m->id;

                        continue;
                    }
                    else {
                        $ar = new AccountRow();
                    }
                }
                else {
                    $ar = AccountRow::find($account_row_id);
                }

                $ar->movement_id = $id;
                $ar->account_id = $accounts[$index];
                $ar->user_id = $users[$index];
                $ar->section_id = $sections[$index];
                $ar->notes = $notes[$index] ?: '';

                if( !$ar->user_id ) {
                  $ar->user_id = null;
                }
                if( !$ar->section_id ) {
                  $ar->section_id = null;
                }

                $amount = (float)$amounts[$index];
                if ($amount > 0) {
                    $ar->amount_in = abs($amount);
                    $ar->amount_out = 0.0;
                } else {
                    $ar->amount_in = 0.0;
                    $ar->amount_out = abs($amount);
                }

                $ar->save();

                if (!isset($rows[$id])) {
                    $rows[$id] = [];
                }

                $rows[$id][] = $ar->id;
            }

            foreach($rows as $movement_id => $rows_id) {
                AccountRow::where('movement_id', $movement_id)->whereNotIn('id', $rows_id)->delete();

                $m = Movement::find($movement_id);
                $m->generateReceipt();
            }
        }

        // Auto-promote to "reviewed" only if sums make sense.
        // Or, downgrade to "to be reviewed" if a sum does not make sense.
        // Historically, a Movement was "reviewed" with at least an AccountRow.
        foreach( $movements_by_id as $movement ) {
            $movement->reviewed = $movement->isAmountMatchingAccountRows() ? 1 : 0;
            $movement->save();
        }

        DB::commit();

        // This controller is used from the review page, that manages multiple Movements.
        if (count($movements_by_id) > 1) {
            return redirect()->route('movement.index');
        }

        // This controller can be used to edit a single Movement. For them, re-open it.
        return redirect()->route('movement.edit', $id);
    }

    public function review()
    {
        $this->checkAuth();
        $pendings = Movement::where('reviewed', 0)->orderBy('date', 'asc')->paginate(20);
        return view('movement.review', compact('pendings'));
    }

    public function attach(Request $request, $id)
    {
        $this->checkAuth();
        $movement = Movement::find($id);
        $movement->attachFile($request->file('file'));
        return redirect()->route('movement.edit', $id);
    }

    public function refund(Request $request, $id)
    {
        $this->checkAuth();

        $ids = $request->input('refund_id');
        $refund = null;

        DB::beginTransaction();

        foreach($ids as $refund_id) {
            $refund = Refund::find($refund_id);
            $refund->movement_id = $id;
            $refund->refunded = true;
            $refund->save();
        }

        if ($refund && $refund->section) {
            $movement = Movement::find($id);
            foreach($movement->account_rows as $ar) {
                $ar->section_id = $refund->section_id;
                $ar->save();
            }
        }

        DB::commit();
        return redirect()->route('movement.edit', $id);
    }

    public function download(Request $request)
    {
        $this->checkAuth();
        return response()->download(storage_path('accounting/' . $request->input('file')));
    }

    /**
     * Unicredit has nonsense export amount format.
     * Unicredit amounts change accordingly to your browser language.
     * This tries to understand which one is coming.
     * @param string $raw_number Example "1.000,00" or "1,000.00" - it MUST have decimals.
     * @return string International number like "1000.00"
     */
    private function parseUnicreditFloatNumber($raw_number)
    {
        $clean_number = $raw_number;

        $comma = substr($raw_number, -3, 1);
        if ($comma === ',') {
            // Convert from "1.000,00" to "1,000.00"
            $clean_number = str_replace('.', '',  $clean_number);
            $clean_number = str_replace(',', '.', $clean_number);
        } elseif($comma === '.') {
            // Convert from "1,000.00" to "1000.00"
            $clean_number = str_replace(',', '', $clean_number);
        } else {
            throw new \Exception("Cannot parse comma from Unicredit number: '%s' cannot guess between English or Italian number", $raw_number);
        }

        return $clean_number;
    }
}
