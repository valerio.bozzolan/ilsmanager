<?php

namespace App\Http\Controllers;

use App\Movement;
use App\Bank;
use App\Fee;

use Illuminate\Http\Request;

class BankController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Bank',
            'view_folder' => 'bank'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'name' => 'required|max:255',
            'identifier' => 'required|max:255',
        ];
    }

    protected function requestToObject($request, $object)
    {
        $object->name = $request->input('name');
        $object->type = $request->input('type');
        $object->identifier = $request->input('identifier');
        return $object;
    }

    public function list(Request $request, $id = 0)
    {

        $this->checkAuth();

        $movements_query = new Movement;

        $bank = null;
        $banks = Bank::all();

        if ( $id !== 0 ) {
            $bank = Bank::find($id);
            $movements_query = $bank->movements();
        }

        $movements_query->with('account_rows')->orderBy('date', 'desc');

        // As default, filter by current year.
        // Allow to filter by all years, setting '-'.
        $year = $request->input('year');
        if ( $year === '-' ) {
            $year = null;
        } else {
            $year = (int)$year;
            if ( $year <= 0 ) {
                $year = date('Y');
            }
        }
        if ( $year > 0 ) {
            $movements_query->whereYear('date', $year );
        }

        // As default, pick all Movements.
        // Limit the results as default only if we have no year specified.
        $limit = null;
        if( $year === null || $bank === null ) {
            $limit = 100;
        }
        if ( $request->input('limit') > 0 ) {
            $limit = $request->input('limit');
        }
        if ( $limit > 0 ) {
            $movements_query->take($limit);
        }

        $movements = $movements_query->get();

        // Batch-Download all Fees, related to the available AccountRows.
        // This block avoids "N+1" performance issues.
        $fee_by_account_row = (function() use ($movements) {
            $account_row_ids = [];
            foreach($movements as $movement) {
                foreach($movement->account_rows as $i => $account_row) {
                    $account_row_ids[] = $account_row->id;
                }
            }

            $fees = [];
            if($account_row_ids) {
                $fees = Fee::whereIn('account_row_id', $account_row_ids)
                ->get();
            }

            // Return Fees indexed by AccountRow.
            $indexed_fees = [];
            foreach($fees as $fee) {
                $indexed_fees[$fee->account_row_id] = $fee;
            }
            return $indexed_fees;
        })();

        // Create some Data Transfer Objects to simplify the construction of with Movement and (eventually) its AccountRow.
        $movements_dto = [];
        $is_first_ar = true;
        foreach($movements as $movement) {
            $is_first_ar = true;
            foreach($movement->account_rows as $i => $account_row) {
                $movements_dto[] = [
                    'movement'          => $movement,
                    'account_row'       => $account_row,
                    'account_row_first' => $is_first_ar,
                    'fee'               => $account_row ? $fee_by_account_row[$account_row->id] ?? null : null
                ];
                $is_first_ar = false;
            }
        }

        return view('bank.list', compact('movements_dto', 'id', 'bank', 'banks', 'limit', 'year'));
    }

}
