<?php

namespace Tests\Unit;

use App\Bank;
use App\Movement;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MovementWithDatabaseTest extends TestCase
{
    // This allows SQLite
    use DatabaseMigrations;

    /**
     * Test the Movement unique finder on PayPal.
     *
     * @test
     */
    public function movement_paypal_already_tracked(): void
    {
        $bank = new Bank;
        $bank->name = 'Test Bank from movement_unique_finder()';
        $bank->type = 'paypal';
        $bank->identifier = 'test-bank-paypal-unique-finder-' . rand();
        $bank->save();

        // Create a base PayPal Movement.
        $movement = new Movement;
        $movement->bank_id = $bank->id;
        $movement->amount = 0.99;
        $movement->identifier = 'asdlol-paypal-id';
        $movement->date = '2001-02-03';
        $movement->notes = 'Test PayPal movement from test method: ' . __METHOD__;
        $movement->save();

        // Propose a duplicated PayPal Movement that will be recognized by its bank identifier (and only that).
        $movement2 = new Movement;
        $movement2->bank_id = $movement->bank_id;
        $movement2->identifier = $movement->identifier;
        $movement2->amount = 0.98; // Change the amount a bit
        $movement2->date = '2001-02-04'; // Change the date a bit
        $movement2->notes = 'Test PayPal movement bla bla other note from test method: ' . __METHOD__;
        $this->assertEquals(true, $movement2->alreadyTracked(), 'Test PayPal movement already tracked');
        $this->assertEquals($movement->id, $movement2->alreadyTrackedMovement()->id, 'Test PayPal movement already tracked object ID');

        // Propose a very-similar PayPal movement but with different bank identifier.
        $movement3 = new Movement;
        $movement3->bank_id = $movement->bank_id;
        $movement3->amount = $movement->amount;
        $movement3->identifier = 'something-different-paypal-id-' . rand();
        $movement3->date = $movement->date;
        $movement3->notes = $movement->notes;
        $this->assertEquals(false, $movement3->alreadyTracked(), 'Test PayPal movement not tracked');
        $this->assertEquals(null, $movement3->alreadyTrackedMovement(), 'Test PayPal movement not tracked object');

        // Cleanup
        $movement->delete();
        $bank->delete();
    }

    /**
     * Test the Movement unique finder on Unicredit.
     * Note that Unicredit movements have not an identifier.
     *
     * @test
     */
    public function movement_unicredit_already_tracked(): void
    {
        $bank = new Bank;
        $bank->name = 'Test Bank from movement_unique_finder()';
        $bank->type = 'unicredit';
        $bank->identifier = 'test-bank-unicredit-unique-finder-' . rand();
        $bank->save();

        // Create a base Unicredit Movement (without identifier - since Unicredit does not share identifiers).
        $movement = new Movement;
        $movement->bank_id = $bank->id;
        $movement->amount = 0.99;
        $movement->date = '2001-02-03';
        $movement->notes = 'Test Unicredit movement from test method: ' . __METHOD__;
        $movement->save();

        // Propose a duplicated Unicredit Movement.
        $movement2 = new Movement;
        $movement2->bank_id = $movement->bank_id;
        $movement2->amount = $movement->amount;
        $movement2->date = $movement->date;
        $movement2->notes = $movement->notes;
        $this->assertEquals(true, $movement2->alreadyTracked(), 'Test Unicredit movement already tracked');
        $this->assertEquals($movement->id, $movement2->alreadyTrackedMovement()->id, 'Test Unicredit movement already tracked object');

        // Propose another Unicredit Movement that must be considered different.
        $movement3 = new Movement;
        $movement3->bank_id = $movement->bank_id;
        $movement3->amount = $movement->amount;
        $movement3->date = '2002-03-04'; // This date is different from the original movement
        $movement3->notes = 'Test different Unicredit asdlol new movement from test method: ' . __METHOD__;
        $this->assertEquals(false, $movement3->alreadyTracked(), 'Test Unicredit movement not tracked');
        $this->assertEquals(null, $movement3->alreadyTrackedMovement(), 'Test Unicredit movement not tracked object');

        // Cleanup
        $movement->delete();
        $bank->delete();
    }
}
