<?php

namespace Tests\Unit;

use App\AccountRow;
use PHPUnit\Framework\TestCase;

class AccountRowTest extends TestCase
{
    /**
     * @test
     */
    public function amounts_in_out(): void
    {
        $ar = new AccountRow;
        $ar->amount = 100.00;
        $this->assertEquals(number_format(100, 2), number_format($ar->amount_in, 2), 'Account Row in amount');
        $this->assertEquals(number_format(0, 2), number_format($ar->amount_out, 2), 'Account Row out amount');
    }
}
